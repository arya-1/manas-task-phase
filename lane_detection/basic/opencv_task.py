import cv2 
import numpy as np

def display_lines(image, lines):
    line_img = np.zeros_like(image)
    if lines is not None:
        for x1, y1, x2, y2 in lines:
            cv2.line(line_img, (x1,y1), (x2,y2), (63,1,249), 10)
    return line_img


def canny(image):
    gray = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
    blur = cv2.GaussianBlur(gray, (5,5), 0)
    canny = cv2.Canny(blur, 50, 150)

    return canny

def ROI(image):
    h = image.shape[0] #rows X cols
    polygons = np.array([[(200,h), (1100, h), (550, 250)]])

    mask = np.zeros_like(image)
    cv2.fillPoly(mask, polygons, 255)

    masked_image = cv2.bitwise_and(image, mask)
    return masked_image


def make_coordinates(image, line_parameters):
    slope, intercept = line_parameters
    y1 = image.shape[0]
    y2 = int(y1 * (3/5))
    x1 = int((y1 - intercept) / slope)
    x2 = int((y2 - intercept) / slope)

    return np.array([x1,y1,x2,y2])

def average_slope_intercept(image, lines):
    left_fit = []
    right_fit = []

    for line in lines:
        x1, y1, x2, y2 = line.reshape(4)
        slope, intercept = np.polyfit((x1,x2), (y1,y2), 1)

        if slope < 0:
            left_fit.append((slope, intercept))
        else:
            right_fit.append((slope, intercept))
    
    left_fit_avg = np.average(left_fit, axis=0)
    right_fit_avg = np.average(right_fit, axis=0)

    
    left_line = make_coordinates(image, left_fit_avg)
    right_line = make_coordinates(image, right_fit_avg)

    return np.array([left_line, right_line])



def main():
    cap = cv2.VideoCapture("vid1.mp4")
    while cap.isOpened():
        x, frame = cap.read()
        canny_img = canny(frame)
        cropped_img = ROI(canny_img)
        lines = cv2.HoughLinesP(cropped_img, 2, 5 * np.pi/180, 100, np.array([]), minLineLength=10, maxLineGap=400)
        lines = average_slope_intercept(frame, lines)
        line_img = display_lines(frame, lines)
        combined_img = cv2.addWeighted(frame, 0.8, line_img, 1, 0)
        cv2.imshow('lines', combined_img)
        cv2.waitKey(1)

    cap.release()
    cv2.destroyAllWindows()
if __name__ == '__main__':
    main()
