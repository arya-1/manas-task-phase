import cv2
import numpy as np

class PerspectiveTransformation:
    def __init__(self):
        self.src = np.float32([(550, 460),     # top-left
                               (150, 720),     # bottom-left
                               (1200, 720),    # bottom-right
                               (770, 460)])    # top-right
        self.dst = np.float32([(100, 0),
                               (100, 720),
                               (1100, 720),
                               (1100, 0)])
        
        self.M = cv2.getPerspectiveTransform(self.src, self.dst)
        # print(self.M)
        self.M_inv = cv2.getPerspectiveTransform(self.dst, self.src)

    def forward(self, img, img_size=(1280, 720), flags=cv2.INTER_LINEAR):
        return cv2.warpPerspective(img, self.M, img_size, flags=flags)

    def backward(self, img, img_size=(1280, 720), flags=cv2.INTER_LINEAR):
        return cv2.warpPerspective(img, self.M_inv, img_size, flags=flags)

    def draw(self, img):
        circle_img = np.zeros_like(img) 
        for val in self.src:
            cv2.circle(circle_img, (val[0], val[1]), 10, (0,0,255), -1)
        return cv2.addWeighted(img, 1, circle_img, 1, 0)

if __name__ == '__main__':
    ps = PerspectiveTransformation()
    cap = cv2.VideoCapture('vid1.mp4')
    while cap.isOpened():
        _, frame = cap.read()
        warped_img = ps.forward(frame)
        src_points = ps.draw(frame)
        cv2.imshow('warp', warped_img)
        cv2.imshow('normal', src_points)
        if cv2.waitKey(10) == ord('q'):
            break
    cap.release()
    cv2.destroyAllWindows()