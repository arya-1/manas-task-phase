import cv2
import numpy as np


class Canny:
    def forward(self, img):
        gray = cv2.cvtColor(img, cv2.COLOR_RGBA2GRAY)

        blur = cv2.GaussianBlur(gray, (5,5), 0)

        canny = cv2.Canny(blur, 60, 100)

        return canny

if __name__ == '__main__':
    c = 0
    canny = Canny()
    cap = cv2.VideoCapture('vid1.mp4')
    while cap.isOpened():
        _, frame = cap.read()
        if c == 100:
            cv2.imwrite('lane_image_1.jpg', frame)
        canny_img = canny.forward(frame)
        cv2.imshow('normal', frame)
        cv2.imshow('canny', canny_img)
        if cv2.waitKey(10) == ord('q'):
            break
        c += 1

    cap.release()
    cv2.destroyAllWindows()