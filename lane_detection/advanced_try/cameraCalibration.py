import numpy as np
import cv2
import glob

class CameraCalibration():
    def __init__(self, img_dir, nx, ny):
        fnames = glob.glob("{}/*".format(img_dir))
        objpoints = []
        imgpoints = []

        # Coordinates of chessboard's corners in 3D
        # prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
        objp = np.zeros((nx*ny, 3), np.float32)
        objp[:,:2] = np.mgrid[0:nx, 0:ny].T.reshape(-1, 2)

        for f in fnames:
            img = cv2.imread(f)
            gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)

            # returns the chessboard corners in the photo 
            ret, corners = cv2.findChessboardCorners(img, (nx, ny))

            if ret:
                imgpoints.append(corners)
                objpoints.append(objp)
            
        # returns the camera matrix
        # return the distortion coeffs
        shape = (img.shape[1], img.shape[0])
        ret, self.mtx, self.dist, _, _ = cv2.calibrateCamera(objpoints, imgpoints, shape[::-1], None, None)

        if not ret:
            raise Exception("Unable to calibrate camera")

    def undistort(self, img):
        gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
        return cv2.undistort(img, self.mtx, self.dist, None, self.mtx)


if __name__ == '__main__':
    calibration = CameraCalibration('camera_cal', 9, 6)
    img = cv2.imread('camera_cal/calibration1.jpg')
    undist = calibration.undistort(img)
    cv2.imshow('undistorted', undist)
    cv2.imshow('distorted', img)
    cv2.waitKey(0)
    cv2.destroyWindow()
